#!/bin/bash

set -e

# install OpenJDK dependency
apt-get -y install openjdk-8-jdk

# install build requirements
apt-get -y install pkg-config zip g++ zlib1g-dev unzip python

# install bazel
wget https://github.com/bazelbuild/bazel/releases/download/0.5.1/bazel-0.5.1-installer-linux-x86_64.sh
chmod +x bazel-0.5.1-installer-linux-x86_64.sh
./bazel-0.5.1-installer-linux-x86_64.sh
mv /usr/local/bin/bazel /usr/bin
rm bazel-0.5.1-installer-linux-x86_64.sh

# install envoy build + test requirements
apt-get -y install libtool cmake realpath m4 automake

# install envoy-api dependencies
apt-get -y install protobuf-compiler libprotobuf-dev

chown -R vagrant:vagrant /home/vagrant
